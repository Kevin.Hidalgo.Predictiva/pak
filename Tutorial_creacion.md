# Creación de paquetes en python-Kevin Hidalgo-01102019


<!-- TOC -->
- [Creación de paquetes en python-Kevin Hidalgo-01102019](#creaci%c3%b3n-de-paquetes-en-python-kevin-hidalgo-01102019)
    - [creación de paquetes python](#creaci%c3%b3n-de-paquetes-python)
    - [creación de repositorios en GitLab](#creaci%c3%b3n-de-repositorios-en-gitlab)
    - [Instalación con git+GitLab](#instalaci%c3%b3n-con-gitgitlab)
    - [Enlaces útiles](#enlaces-%c3%batiles)

***
### creación de paquetes python

una de las cosas más poderosas acerca de la codificación de las ciencias es que no cuesta nada volver a utilizar el código que hemos escrito en el pasado, que nos permite construir sobre el trabajo pasado en lugar de empezar de nuevo cada proyecto o papel.
Sin embargo, una práctica que vemos una y otra vez es copiar y pegar el código de un proyecto a otro. a veces sólo será una función.
el montaje de código en paquetes hace que sea muy fácil de reutilizar código antiguo: todas las secuencias de comandos y funciones terminan en una ubicación central y se pueden llamar e importados desde cualquier lugar en el equipo - al igual que los famosos paquetes **numpy** or **matplotlib**

![ejemplo](https://cdn.programiz.com/sites/tutorial2program/files/PackageModuleStructure.jpg)

![ejemplo2](https://www.tutorialsteacher.com/Content/images/python/package.png)

la estructura de directorios más básico para un paquete python se ve así:

~~~
project
|
|__ setup.py
|
|__ myPackage
     |
     |_  somePython.py
     |_  __init__.py
~~~

lo primero será crear un folder con el nombre de nuestro paquete y mover allí los archivos python, ejemplo

~~~
project
|
|__ measure
     |__ norms.py
     |__ metrics.py
~~~

luego tenemos un archivo mucho más importante que es: $__init__.py$ le permite al intérprete de Python que hay módulos se pueden importar en este directorio. este es el script que se ejecute cuando se ejecuta $import measure$ para más información sobre lo que puede hacer con los módulos, se puede ver la [Python docs](https://docs.python.org/3/tutorial/modules.html) después de añadir $__init__.py$ el directorio del proyecto nos debe quedar

~~~
project
|
|__ measure
     |__ __init__.py
     |__ norms.py
     |__ metrics.py
~~~

En este punto, la biblioteca se pueden importar si estamos en el mismo directorio, pero no es un paquete. Debemos permitir que $setuptools$ y $pip$ sepan cómo manejarlo
tenemos que añadir el archivo $setup.py$. una versión muy básica de $setup.py$ es

~~~
from setuptools import setup

setup(
    # Needed to silence warnings (and to be a worthwhile package)
    name='Measurements',
    url='https://github.com/jladan/package_demo',
    author='John Ladan',
    author_email='jladan@uwaterloo.ca',
    # Needed to actually package something
    packages=['measure'],
    # Needed for dependencies
    install_requires=['numpy'],
    # *strongly* suggested for sharing
    version='0.1',
    # The license can be anything you like
    license='MIT',
    description='An example of a python package from pre-existing code',
    # We will also need a readme eventually (there will be a warning)
    # long_description=open('README.txt').read(),
)
~~~

Luego nuestro repositorio tendrá la siguiente forma:

~~~
project
|
|__ setup.py
|
|__ measure
     |__ __init__.py
     |__ norms.py
     |__ metrics.py
~~~

Para generar actualizaciones deberemos no solo actualizar el archivo nuevo o modificado sino también modificar el setup.py en el apartado del versionamiento, luego realizar los commit's e instalar nuevamente el paquete, es probable que requiera el reinicio del kernel de python para que tome los cambios.

### creación de repositorios en GitLab

En **SharePonit** contamos con documentación sobre git en los siguientes enlaces:
* [GitLab](https://predictiva.sharepoint.com/:w:/s/Analtico/ER5XvDuXm0ZDqniGuDtmyowBY4yPvpUhVBi0wWxdlL7cOw?e=Ndq4Vc)
* [conceptos git](https://predictiva.sharepoint.com/:b:/s/Analtico/EeE8WJBn6WBNtbqxbfoKWxgB0P2MtW3iOcmcEGwoS1rPRQ?e=Fga7kM)

Continuando tendremos que hacer push al repositorio en GitLab como se ve acontinuación

![Push](https://predictiva-my.sharepoint.com/personal/kevin_hidalgo_predictiva_com_co/Documents/KevHidalgo/Proyectos/PGitlab/push.png)

ahí veremos que nos aparece la url que luego utilizaremos para instalar nuestro paquete de **Python**

### Instalación con git+GitLab

esta es mi forma preferida de paquetes compartir / almacenamiento / instalación. simplemente crear un repositorio git del directorio del proyecto y ponerlo en algún lugar. a continuación, utilizar PIP para instalarlo desde el repositorio directamente.

En la consola de __Anaconda Prompt__ escribiremos

~~~
pip install git+https://gitlab.com/Kevin.Hidalgo.Predictiva/pak.git@master
~~~

**Ejemplo en python**

~~~
from prueba.function import sum
sum(10,20)

from prueba.function import power, average
from prueba.greet import SayHello

SayHello("nombre")
x=power(3,2)
print("power(3,2) : ", x)
average(10,12)
~~~

### Enlaces útiles

* [CREATING PACKAGES IN PYTHON](https://uoftcoders.github.io/studyGroup/lessons/python/packages/lesson/)

* [How To Package Your Python Code](https://python-packaging.readthedocs.io/en/latest/index.html)

* [Python Code](https://www.tutorialsteacher.com/python/python-package)

* [stackoverflow-gitlab](https://stackoverflow.com/questions/46767851/gitlab-able-to-clone-but-not-pip-install-directly)

* [using-private-packages-python](https://www.revsys.com/tidbits/using-private-packages-python/)